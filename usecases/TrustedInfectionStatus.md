# Introduction
A person’s ‘infected status’ in relation to COVID-19 is an essential element of any COVID-19 interaction tracing method. The purpose of this document is to outline the use case for ‘trusted infection status reporting’ focused on the specific situation in The Netherlands.

> We are working to get the contents of this document validated by healthcare professionals and will integrate their feedback as soon as we can. We welcome other 'COVID-19 related app developers' to make use of this document. Please share your insights so we can use this document as a knowledge repository and limit the load on our healthcare professionals.


# Infection Status
Whilst a person’s infection status can be unambiguously defined as a person carrying the COVID-19 virus, there are various assessment methods with different levels of certainty, ranging from a test in an authorized laboratory to a questionnaire based self-assessment. 
For the purpose of interaction tracing, the following infection statuses are proposed:


| status       | description[1][1]                                                                                                                                                                                                                                                        | update authorisation    |
| ------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ----------------------- |
| Confirmed    | A person with laboratory confirmation of COVID-19 infection, irrespective of clinical signs and symptoms.                                                                                                                                                          | Healthcare professional |
| Suspect      | A patient with acute respiratory illness, and 1) history of travel to location with known infections, or 2) having been in contact with a person with a confirmed or probable case in last 14 days, or 3) being hospitalized and absence of alternative diagnosis. | Healthcare professional |
| Probable     | A suspect case for whom testing for the COVID-19 virus is inconclusive, or, a suspect case for whom testing could not be performed for any reason.                                                                                                                 | Healthcare professional |
| Possible     | A person meeting the criteria for a potential infection based on a self-test questionnaire published by healthcare authorities, but not confirmed by a healthcare professional.                                                                                    | Individual              |
| Not infected | A person not falling into any of the other categories.                                                                                                                                                                                                             | n/a                     |

*Table 1: overview infection statuses*
# Proposed implementation
For the (initial) implementation of the interaction tracing method is it proposed to focus on the ‘confirmed’ and ‘not infected’ statuses only. Whilst this does not reflect the full range of uncertainty, it will reduce the probably of false positives significantly and will simply and therefore accelerate the first implementation.

 It is recommended to take the full range of infection statuses into account in any infecting status coding scheme and balance the consequences of creating flexibility with other factors, such as development time, compatibility with other solutions, privacy risks and performance & network impact.

# Testing and reporting of infected status
## Workflow

The following sequence diagram provides an overview of the testing process and the related infection status update in case of a positive test result.

![testing process](images/infected-status-reporting-sequence-diagram.svg)  
*Figure 1: testing process*

*[sequence diagram source](infected-status-reporting-sequence-diagram.txt)*

The above process does not indicate elapse time, but it is imperative that the whole process timeline is condensed as much as possible and no time is lost during any of the process steps. It is also important to assess the ‘normalised reference date’ (to be defined) for a specific person, as the elapse time of the whole testing process may vary case by case and could impact the interaction tracer logic.  

It should also be noted that an individual has the right to chose not to know the outcome of his test.

## Reporting infected status

In order to avoid false positive infected status uploads the authentication and authorization by a health professional is a key element in the ‘reporting infected status’ part of the testing process.  
The following requirements for the uploading of an infected status are proposed:

- An authorized healthcare professional needs to confirm the infected status (‘healthcare upload approval’);
- The healthcare upload approval can only be used for the specific infection case;
- The uploaded infection data cannot be traced back to a specific individual.

Healthcare professionals in the Netherlands make use of the ‘UZI-register’ (‘Unieke Zorgverlener Identificatie register’) for authentication and identification by use of an ‘UZI-pas’. The UZI-pas contains a PKI-certificate that can be used for authentication and identification of an individual healthcare professional against the central register. Integration with the interaction tracer method system should be straightforward from a technical perspective.  



![UZI pas](images/Uzi-pas.jpg)  
*Figure 2: UZI pas*


The following professions (‘beroepen’) are entitled to obtain a healthcare-professional UVI-pas (‘zorgverlenerspas’), as per article 34, sub 3 of ‘Wet BIG’[2][2]:

| Artikel 3                                     | Article 3                               |
| --------------------------------------------- | --------------------------------------- |
| Apotheker                                     | Pharmacist                              |
| Arts                                          | Doctor                                  |
| Fysiotherapeut                                | Physiotherapist                         |
| Gezondheidszorgpsycholoog                     | Healthcare                              |
| Orthopedagoog-generalist                      | Orthopedagogist generalist              |
| Physician assistant                           | Physician assistant                     |
| Psychotherapeut                               | Psychotherapist                         |
| Tandarts                                      | Dentist                                 |
| Verloskundige                                 | Midwife                                 |
| Verpleegkundige                               | Nurse                                   |
|                                               |                                         |
| **Artikel 34**                                | **Article 34**                          |
| Apothekersassistent                           | Pharmacist's assistant                  |
| Diëtist                                       | Dietician                               |
| Ergotherapeut                                 | Occupational therapist                  |
| Huidtherapeut                                 | Skin therapist                          |
| Klinisch fysicus                              | Clinical physicist                      |
| Logopedist                                    | Speech therapist                        |
| Mondhygiënist                                 | Dental hygienist                        |
| Oefentherapeut                                | Exercise therapist                      |
| Optometrist                                   | Optometrist                             |
| Orthoptist                                    | Orthoptist                              |
| Podotherapeut                                 | Podiatrist                              |
| Radiodiagnostisch laborant                    | Radiodiagnostic technician              |
| Radiotherapeutisch laborant                   | Radio-therapeutic laboratory technician |
| Tandprotheticus                               | Dental prosthetician                    |
| Verzorgende in de individuele gezondheidszorg | Caregiver in individual health care     |

It is proposed that out of these professions only a doctor (‘arts’) is authorized to issue a healthcare upload approval. In its simplest from, the approval can be achieved by sending a S/MIME signed email to the GP authorisation API, as the signature can be validated against the root certificate and contains the specific profession of the card holder. Alternative a web interface can be created for entering the data, using the UZI-pas for authentication. 

It is also essential that all patient related medical information (e.g. test id, TestAuth code, test results) is only stored in the existing medical systems for patients data ('EPD').

The requirement to guarantee privacy of the patient at all times will require some further design work, in particular to avoid the use of secondary technical identifiers (e.g. device IP address) for de-anonymisation purposes.



[1]: https://apps.who.int/iris/bitstream/handle/10665/331506/WHO-2019-nCoV-SurveillanceGuidance-2020.6-eng.pdf "link to WHO report"
[2]: https://www.bigregister.nl/registratie/nederlands-diploma-registreren/wet--en-regelgeving "link to BIG register"
