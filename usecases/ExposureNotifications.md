# Introduction

This document details how a user testing positive for the COVID19 virus results in notifications to those that have been exposed to that affected user during while that the affected user was potentially contagious. This is in many ways at the core of any COVID-19 proximity tracing method.

This document is complementary to the [TrustedInfectionStatus](./TrustedInfectionStatus.md) use case which describes the workflow for affected users and focuses on the authorisation by a healthcare professional.

# Requirements

## Exposure Keys Retention and Publishing

Each broadcasting device retains the information needed to be able to later notify users that they may have been exposed to the affected user. This is the `ExposureKeys`. This is a rolling set of data, with `ExposureKeys` older than the retention required as per RIVM guidelines removed on a daily basis. The period to retain such information is called `ExposureKeysRetention`.

When a user engages with a health care professional as per the [TrustedInfectionStatus](./TrustedInfectionStatus.md) use case, the (potentially) affected person publishes the `ExposureKeys` information stored on the device, together with the randomly generated authorisation code (`AuthCode`) that was also shared with the health care professional. This information is stored securely on the central server, awaiting a potential confirmation of a postive test result by a heathcare professional. If later the test result comes back positive, then the health care professional enters the `AuthCode` from the affected user - essentially releasing the ExposureKeys from escrow on the server. In addition, the healthcare professional also enters their best estimate of the first day the patient was contagious (`EstimatedFirstDayContagious`). This date is important as there is a window from this date that an affected user is potentially contagious. This duration is set as per RIVM guideline and is called `ExposureContagiousDuration`.

Upon receiving a valid `AuthCode` and associated `EstimatedFirstDayContagious` date from the health care professional, the server retrieves the associated `ExposureKeys` information. This set of keys is trimmed using the `EstimatedFirstDayContagious` and `ExposureContagiousDuration` parameters, and the resulting `ExposureKeys` is then hashed and inserted into the next generation Cuckoo filter[1] for sharing to all users.

## Exposure Identifier Retention

Each receiving device stores the EphIDs from broadcasting devices together with additional data about that interaction. This is a rolling set of data, with interactions older than the `ExposureIDsRetention` removed on a daily basis. This parameter is set as per RIVM guidelines. 

## Local Identification of Exposure Events

The device periodically perform a secure and verified download of the latest verison of the Cuckoo filter to locally determine whether any detected of the interactions that they have recorded are exposure events. This is done by checking the observed and locally stored hashes against the Cuckoo filter. 

If a match is detected, then the date of the interaction (`ConfirmedExposureDate`) will be calculated and in case of multiple interactions with infected users, the date of the most recent one will be taken.

## Notifiying the user of an Exposure Event

When an Exposure event is detected, then the user is notified and also receives advice on next actions they should take. The notification and advice is as per RIVM guidelines. There are two forms for the advice: `IsolationAdviceShort` for the message box that is shown to the user, and `IsolationAdviceLong` that can be displayed on device if the user wishes to have additional information. 

# Parameters

As per the descriptions above, there are a number of parameters that need to be configurable in the overall solution - both on the server and on the client devices. For the client devices it is critical that the devices are able to retrieve updates to these parameters from the server so that updated RIVM guidelines can be shared to all devices. 

The following parameters are configurable on the clients and can be centrally updated based on advice of the health authorities:

| parameter                     | Description          | Example           |
| ----------------------------- | -------------------- | ----------------- |
| ExposureKeysRetention         | Number of days the exposure keys information is kept on the device before being deleted | 14 days |
| ExposureIDsRetention          | Number of days interaction data is kept on the device before being deleted | 14 days |
| IsolationPeriod               | Number of days to isolate from `ConfirmedExposureDate` | 10 days |
| IsolationAdviceShort          | Short message to alert the user that they have been exposed to an affected user | You have had an interaction with a confirmed infected person, please self-isolate for x days'. |
| IsolationAdviceLong           | More extensive advice and background infromation in html format to be renderded on separate screen in the app. Note: this is stored and rendered locally ||

The following parameters are configurable on the server and can be centrally updated based on advice of the health authorities:  

| parameter                     | Description          | Example           |
| ----------------------------- | -------------------- | ----------------- |
| ExposureContagiousDuration    | Number of days a user is deemed to be contagious from first day when contagious | 5 days | 

## user interface - privacy considerations

Following PrivateTracer's stance on 'fundamental privacy', it is important to note that the actual design of the user interface and communications with the central server around this workflow needs to be carefully reviewed. After the user has taken careful notice of the advice, the application status should be reset to 'normal' to avoid that users can be co-erced into showing information related to their health by third parties (e.g. employers, insurers). Furthermore, it should not be possible to determine any health related information by observing the interaction of the application with any server. for this reason every device will periodically check for an update of all configurable client side parameters, independent of the user status.



[1]: https://github.com/DP-3T/documents/blob/master/DP3T%20White%20Paper.pdff "DP-3T whitepaper"

