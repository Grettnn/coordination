# Exposure Detection Configuration

In order to determine whether an Exposure Detection should be seen as "risky", PrivateTracer can currently measure two things:

- The RSSI level of the BLE service advertisement received by the foreign device
- The duration of the contact, up to the Epoch cycle time (5 minutes)

The client projects have been designed to be configurable and this configuration can be downloaded from the server. The latest ID of the configuration is returned within the JSON dictionary returned by  the `GET /config` call:
```
"exposureConfig": "59fd49a"
```

The current configuration parameters are then returned from `GET /exposureconfig/{id}`:

- RSSI threshold calibration data: list of devices and thresholds for each device. Default: -68

	```{"rssiThreshold": {"DeviceA": -65, "<device identifier B>": -69, "default": -68}}```
		
	**Todo**: Are the thresholds per device meant to be used for the scanning device or the broadcasting device? 
	
	**Todo**: Specify the device identifiers uniquely.

- Minimum duration of a contact (time between first seen and last seen) in seconds. Default: 180

	```{"contactDurationSeconds": 180}```
	
Note: this is a minimal set of exposure detection parameterisation reflecting that the exposure detection element of PrivateTracer has to date focused on enabling the end-end workflows as opposed to fully robust exposure detection. 

The [set of parameters](https://developer.apple.com/documentation/exposurenotification/enexposureconfiguration) supported by the Apple/Google Exposure Detection framework is as follows:

```
{"minimumRiskScore": 0,
 "attenuationLevelValues": [1, 2, 3, 4, 5, 6, 7, 8],
 "attenuationWeight": 50,
 "daysSinceLastExposureLevelValues": [1, 2, 3, 4, 5, 6, 7, 8],
 "daysSinceLastExposureWeight": 50,
 "durationLevelValues": [1, 2, 3, 4, 5, 6, 7, 8],
 "durationWeight": 50,
 "transmissionRiskLevelValues": [1, 2, 3, 4, 5, 6, 7, 8],
 "transmissionRiskWeight": 50}
```

where the `attenuationLevelValues` would have to be dependent on the device requesting them (or could be turned into a device-dependent calibration table).
