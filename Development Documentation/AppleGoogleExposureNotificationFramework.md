# Implications of adopting Apple-Google ExposureNotification Framework
This is a development team working document to detail out the implications for adopting the ExposureNotification Framework as being developed by Apple & Google. 

__The high level summary is that the implications are largely isolated, so it is expected to be very straightforward to migrate PrivateTracer to be based on the ExposureNotificationFramework__. In part as is due to having the DP-3T logic cleanly isolated, and also as the project has already been largely treating the detection of exposure events as a separate layer from the start. 

## Consequences for PrivateTracer


#### The following parts of PT would remain the same:

- **Server**: The server side functionality, including having both a "lite" and "full" instances of the server side logic remains. And of course the whole Azure deployment path remains, with its option to be hosted in a NL gov approved tenant (as per agreements that are in place between Microsoft & NL government). Clearly some updates are required to be able to move from DP-3T design style 2 back to design style 1 - but again largely isolated due to the modular design of the server.
- **TrustedInfectionStatus**: [The Health Care Authorisation](../usecases/TrustedInfectionStatus.md) use-case remains as is, so the integration with __UZI Pass___ for doctor authorisation remains as is. When the user indicates a positive diagnosis, the list of Diagnosis Keys is retrieved from the system. Then we would still generate an authorization code to be sent along with the keys to the server, so the doctor can confirm the diagnosis
- **Notifications**: Users would still regularly download the Diagnosis Keys from the server and pass them to the framework to check for risky interactions.
- **Clients**: The clients and the user interface are still largely as is as the underlying protocol and exposure detection mechanims were isolated from the user: The only thing we need to do is to start/stop the Exposure Notification service when the user clicks the "Switch on/off" button
- **RIVM Configurable Advice**: [The Configurable Advice](../usecases/ExposureNotifications.md) use case is also not affected, as the RIVM related parameters on the client that are updatable via versioning processes on the server are independent of the ExposureNotification framework. That said, there may be more flexibility offered that can be leveraged e.g. by having multiple advices tailored to the various risk levels. 

#### The following parts of PT would need to be replaced by the ExposureNotification Framework:

The DP3T library can be entirely removed, as can the Bluetooth stack and exposure detection logic 



#### Server-side changes:

- A much small set of Diagnosis Keys to be stored and published
- No Cuckoo Filter generation, the Diagnosis Keys that are uploaded are downloaded unchanged by the users.
- Support for the new features below

#### New features / further investigation:

In addition there are some interactions that need to be further investigated:

- The app is not able to add user interaction to the contact tracing flow itself, that's done by the system in the background and when a contact is detected there's no way for the app to know.
- When checking for risky exposures, the framework returns a list of exposures with their risk level. **Of each exposure, it will tell you the date, duration and risk score**, so this might be an opportunity to inform the user and let him/her decide on the real risk. It's not clear to me how significant this might be a **privacy breach**, as it makes it relatively easy for people to trace back to positively diagnosed contacts.
- To determine whether or not an interaction should be deemed high risk, the framework provides with the possibility pass in custom parameters into the risk determination algorithm. For each Diagnosis Key (on the server), a health care authority could add risk parameters, e.g. how contagious is someone (based on age or symptoms for example). That risk factor is downloaded along with each Diagnosis Key.
- It is possible to chain contact tracing: So if someone, after downloading Diagnosis Keys, is determined to be highly at risk of also beeing positive, to upload **their Diagnosis Keys** with an associated secondary risk factor, that would propagate. However it is not clear to me how to properly secure this as a real positive diagnosis would not be involved (hence no doctor approval).
- The other parameters affecting the risk calculation are the **duration** of an exposure, the number of **days** in the past the exposure occured and the **bluetooth signal strength**. These are **configurable** so they can be dynamically downloaded from RIVM for example (as already implemented for PrivateTracer with the **RIVM Configurable Advice**)

## Technical Analysis

Apple mentions explicitly that **only one app per country** will be allowed. In NL this is expected to be VWS. For now analysis is based on reference documentation that has been made available as well as the reference implementations for applicaitons that have been published . 

### Bluetooth
The main advantage (and goal) of the Apple/Google framework is to enable leveraging of the Bluetooth LE Advertising Payload, even when the app is not running. This is very similar to what PrivateTracer is doing currently on Android, but wasn't possible on iOS. Some notable differences:

- The Exposure Notification Service would receive a standardized ID: `0xFD6F`, registered with the Bluetooth SIG (Special Interest Group) and therefore reserved for this specific purpose
- The Advertising payload carries service data (this wasn't possible on iOS, only for a few specific services, now EN is added to the list) which contains the "Rolling Proximity Identifier" (equivalent to the "ephemeral identifier" in DP3T)
- The Adverstising payload also carries 4 bytes of encrypted data allowing versioning of the protocol and passing the transmit power level which will enable much more accurate calibration of distance measurement.
- The advertisements are non-connectable so other devices can't try to drain battery by attempting a connection
- The advertisements are broadcast every ca. 250ms
- The device also changes **its own advertiser address** when the Rolling Proximity Identifier changes, so these identifiers cannot be tracked over time.

Detailed information [here](https://covid19-static.cdn-apple.com/applications/covid19/current/static/contact-tracing/pdf/ExposureNotification-BluetoothSpecificationv1.2.pdf)

### Rolling Identifiers

The main take-away is that this is similar to version 1 of the DP3T protocol, as it was back in March - and which PT had operational then (basis for first demonstrations). The move to using DP-3T design 2 looks like work that we will for now be shelving. 

The implications from updating the current design are:

- Temporary Exposure keys are generated once a day. When someone is diagnosed with COVID-19, these are the keys that are uploaded to the server (a subset of them called "Diagnosis Keys", depending on how many days back the infection is deemed contagious)
- The Temporary Exposure keys are used to generate the **Rolling Proximity Identifiers** every 15 minutes. These are exchanged with other mobile devices over Bluetooth.
- When someone is diagnosed with COVID-19, her Diagnosis Keys are uploaded to a server, then downloaded by everyone else. These can be used to re-generate all the Rolling Proximity Identifiers that would have been broadcasted, so devices can check locally if they have a match.

Detailed information about the generation of Rolling Identifiers can be found [here](https://covid19-static.cdn-apple.com/applications/covid19/current/static/contact-tracing/pdf/ExposureNotification-CryptographySpecificationv1.2.pdf)


### Exposed APIs
The Exposure Notification framework **does not** expose the Bluetooth API to craft the broadcasted advertisements. For PrivateTracer this means:

- We cannot use this to put our own payload in the BT advertisements
- We have no influence on how the "rolling proximity identifiers" are generated

On a high level, the Exposure Notification framework exposes 3 main things:

- Starting/stopping of the exposure notification service, i.e. start/stop broadcasting and tracing (iOS: `ENManager.setExposureNotificationEnabled()`)
- Retrieving the Temporary Exposure Keys from the framework when the user confirms a positive diagnosis (iOS: `ENManager.getDiagnosisKeys()`).
- Passing a set of Diagnosis Keys received from the server to the framework and asking the framework to run a check against all registered contacts (iOS: `ENManager.detectExposures()`).

More info [here for iOS](https://developer.apple.com/documentation/exposurenotification/enmanager)

At the system level, the user will have two moments at which he/she will have to authorize the app to do things:

- When starting the service, the user will be asked by the system to authorize Exposure Notification tracing.
- When the app wants to retrieve the Temporary Exposure Keys, the user will be required to confirm.

### Additional Information
- There are reference implementations of Apps using the ExposureNotificationFramework for both [iOS](https://developer.apple.com/documentation/exposurenotification/building_an_app_to_notify_users_of_covid-19_exposure) & Android
- The API reference material & design documents are online, [iOS docs here](https://developer.apple.com/documentation/exposurenotification)
