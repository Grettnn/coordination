# Signing commits from Visual Studio

1. Generate a PGP key using the PGP tooling of your choice. A useful guide is [here](https://jamesmckay.net/2016/02/signing-git-commits-with-gpg-on-windows/)
2. Note you do not have to globally set all commits to be signed. In your local Git config file in the .git folder of the repo:

[commit]

        gpgsign = true

[user]

        name = <the name you used generating the GPG key>
        email = steve.kellaway@mefitihe.com


3. Add a copy of your public key to your GitLab account (I believe this step is necessary to sign commits when directly editing via the website). See Profile, Settings, GPG Keys.
