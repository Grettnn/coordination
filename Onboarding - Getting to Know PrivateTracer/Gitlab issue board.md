# Gitlab issue board
All issues are put up on the Gitlab issue [boards](https://gitlab.com/groups/PrivateTracer/-/boards/1641202)  
At regular meetings the progress of the issues (at least those in the `doing` column) is briefly discussed and the board updated accordingly. The issues and the various related boards that have different filters on the backlog represent the current shared understanding of the work in progress / planned in the project. 

## How to use this board?
- New issues can be made in the `open` column (not refined yet) or `todo` (refined & ready to pick it up).
    - Issues should have the relevant tags assigned: components (orange e.g. `Android`), relevant workflows (blue, e.g. `InfectedUserStatus`) and if relevant non-functional (e.g. `security` ) or community tags (e.g. `on-boarding`)
- *When you'd like to start with an issue, assign yourself to it and move the task to `doing`, so we won't work on the same thing twice.*
    - All issues in the `doing` column should have at least one person assigned (to refine later if max one person)
- Issues can be `blocked` only when they are at least refined. Unrefined isn't the same as `blocked`.
- Issues can be moved to `done` when the code is merged.

There are also multiple pre-configured issue boards that filter for now primarily on the tags assigned to issues - please ensure you look at all relevant boards. For example if you are working on an issue tagged both as `security` and `InfectedUserStatus` then make sure the wider context for both those tags is understood. 

Finally, the process around the board usage is evolving - if you have suggested improvements, or simply want to help out, please let us know!
