# Resources needed

* **Client.iOS**: senior Swift developers: UI, business logic, robustness - details on the [board](../boards/1686114?&label_name[]=iOS). Probably our top priority at the moment...

* **Server**: while the functionality is conceptually straightforward, layering the solution to ensure robustness, isolating dependencies & role access etc all requires strong full stack developers, especially Azure. If up for it please drop a line!

* **DevOps**: as the project moves towards production quality, the role of DevOps is critical to assure quality and continued team effectiveness. Plenty of areas you can help out - especially if you are familiar with Azure

* **QA & Testing**: code quality is in place, but there is an on-going need for additional testing as well as formalising the  QA processes / gap analysis & planning. The solution is targeting a huge roll out, so it has to be rock solid. 

* **Product Owners** while the core UZI pass integration and the associated doctors authorisation of infected status (via proxy for privacy) - there are many edge cases that need to be understood, documented and implemented. If you have experience in the medical space, then please let us know so we can ensure the domain knowledge is directly tied into the developoment team activities.

* **Client.Android**: always a lot to be done, to round out the application. If you are a senior android developer please have a look at our boards & see if there are cards there that you are keen to help with!

The boards are the central information flow for the project - if any questions on the above or to get the latest overview of what needs to be done then please look at the various boards. 
