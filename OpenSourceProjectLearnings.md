# Open Source project learnings
It has been an intense journey, from initial discussions on how to help society to effectively deal with the Covid19 virus, through to having a running and in our opinion effectively setup open source community working on this topic.

Open source has been the approach from day one, and that included doing all the delivery coordination and alignment also via the public 
repository - sounds straightforward and clearly is something that happens widely in the open source world - but there is a lot that goes into creating an 
effective setup that perhaps is not always visible from the outside.

Several of those coordinating the project have significant "corporate" software engineering excellence experience, and the developers contributing were strong, knowledgabe & pragmatic - and while setting up an open source community project 
shares many aspects, there are also differences in approach / choices that have taken a lot of discussions and actions.

This short note is intended to share some of those learnings, as well as the places where we have faced challenges.

## Aspects
A selection of the topics that have been worked through:
- **Repository Hosting**: Based on dicussions with some experienced open source friends, in the end the choice was made to go for GitLab over GitHub due to more being "out of the box". Clearly not much in it as GitHub is also an excellent option, but GitLab has worked well for the project
- **Open Source Registration**: Once established on GitLab, then it became rapidly important to be able to leverage more of their functionality than is available in the "free" offering. Thankfully they do support open source initiatives, so we engaged on getting the project verified as open source. Sounds easy, but ended up taking a lot of emails / discussions before it did get in place. 
- **DevOps (CI/CD)** - set up multiple development environments and create CI/CD pipelines, as well as NuGet packaging servers etc.
  - Azure DevOps - Azure DevOps pipelines can be used, with pipelines saved as .yaml files to the public (gitlab) repository. This way a seemless integration with Azure is possible whilst maintaining  full open source transparency.
- **License Choice** - open source is a broad term, with many flavors available. After weighing how far to go "copy-left", in the end the choice was made for the MIT License as that keeps as many options open moving forward in the futuer (can go from there more copy-left, but not the other way around)
- **Legal Contributions (Contributor License Agreement)** - A formal way to declare the right to make contributions to the project using a developer friendly workflow [5][5].). Multiple discussions with lawyers about how to ensure that the code that is delivered can be re-used safely without fear of legal implications - and after weighing up approaches in the end the choice was made to use a lightweight CLA process using verified identities to ensure that this aspect is "sufficiently" covered.
- **Responsible disclosure** - clear [responsible disclosure policy](ResponsibleDisclosure.md) enabling the efficient and secure disclosure of vulnerabilities.
- **Security aspects**[1][1], including access & identity management - a set of '*least privilege*' principles around individual access rights and account and code integrity (e.g. 2-factor authentication, signed commits) [2][2].
- **Code quality** - regular automated code scanning for various aspects of software maintainability [3][3]
- **Community onboarding** - a clear set of guidelines for onboarding new developers [4][4]
- **Project coordination** - the use of issues board to document, prioritise and collaborate on outstanding tasks [5][5]
- **Collaboration environment** - the use of an online collaboration tools (including text, graphics, video/voice) for the project community to openly share dilemma's, concerns, news, ideas and progress (in case of PrivateTracer a combination of [Discord](https://discord.com/) and [Jitsi](https://jitsi.org/)).

## Alignment
In any such community of volunteers it is critical that there are pragmatic & lightweight means to ensure that understanding and focus are aligned. To that end the project uses the following
- **Daily standups** - daily standups are a common best practice. We found it useful to have a seperate standup for security/privacy.
- **Discord** - Multiple channels aligned with the core use cases & components used in the project
- **Terminology / Labels** - clear set of labels and terms used throughout the project
- **Roles** - clear roles on the project, knowing who to contact for what topic
- **Culture** - ensuring the interactions remain open, respectful and constructive throughout

## Security, Ethics & Privacy
These aspects have to be build from the ground up in the project, not added on as an afterthought. As such these are full separate labels and boards used in the project, to ensure the work on these aspects is aligned, visible and prioritised.
- **Security by design**
  - Embeding of security experts in all workstreams to ensure security and privacy aspects are addressed as part of the development ('*DevSecOps*').
  - create 'security challenges' for developers to  embed common security practices (e.g. 2FA, commit signing)
- **DPIA** - Create a 'Data Protection Impact Assessment' at an early project stage as writing it will aid the 'privacy by design' approach.
- **Expert advice** - access to industry recognised independent advisors (external to the projects) for challenge and support.


[1]: https://gitlab.com/PrivateTracer/coordination/-/blob/master/Security/README.md
[2]: https://gitlab.com/PrivateTracer/coordination/-/blob/master/Security/IAM.md
[3]: https://gitlab.com/PrivateTracer/coordination/-/blob/master/SoftwareQualityStatus.md
[4]: https://gitlab.com/PrivateTracer/coordination/-/tree/master/Onboarding%20-%20Getting%20to%20Know%20PrivateTracer
[5]: https://gitlab.com/groups/PrivateTracer/-/boards
[6]: https://gitlab.com/PrivateTracer/coordination/-/tree/master/cla
