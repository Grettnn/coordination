# Software quality (maintainability) assessment

PrivateTracer is using the Sigrid platform from SIG to assess the technical quality (Maintainability) of the software of all sub-projects of the end-end solution. This results in an overall quality score per sub-project.  

We strive to achieve at least 4 stars for every sub-project.

The current assessment of the sub-projects is (last update 2020-04-28):

| sub-project             | score                                                        |
| ----------------------- | ------------------------------------------------------------ |
| Server azure            | ![](https://img.shields.io/badge/SIG%20code%20quality-4%20stars-success) |
| Client.iOS              | ![](https://img.shields.io/badge/SIG%20code%20quality-4%20stars-success) |
| Client.Android          | ![](https://img.shields.io/badge/SIG%20code%20quality-4%20stars-success) |
| Android DP3T library    | ![](https://img.shields.io/badge/SIG%20code%20quality-5%20stars-success) |
| Server Caregiversportal | ![](https://img.shields.io/badge/SIG%20code%20quality-4%20stars-success) |
| DP3T-Rust               | ![](https://img.shields.io/badge/SIG%20code%20quality-5%20stars-success) |


4 stars is defined as "above market average": The maintainability is better than 65% of the rest of the current market.
5 stars means that the maintainability is better than 95% of the rest of the current market.

The full measurement method is ([described in this document](https://www.softwareimprovementgroup.com/wp-content/uploads/2019/11/20190919-SIG-TUViT-Evaluation-Criteria-Trusted-Product-Maintainability.pdf)) 
The metrics measured as part of Maintainability are:
- Volume
- Duplication
- Unit size
- Unit complexity
- Unit interfacing
- Module coupling
- Component balance
- Component independence
- Component entanglement

> We are working on providing a more detailed, dynamic assessment per sub-project.
